package money;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class moneyTests {

    // Tests
    public static void testMultiplication() {
        Main.Money five = Main.Money.dollar(5);
        assertEquals(Main.Money.dollar(10), five.times(2));
        assertEquals(Main.Money.dollar(15), five.times(3));
        assertNotEquals(Main.Money.dollar(16), five.times(3));
    }

    public static void testFrancMultiplication() {
        Main.Money five = Main.Money.franc(5);
        assertEquals(Main.Money.franc(10), five.times(2));
        assertEquals(Main.Money.franc(15), five.times(3));
        assertNotEquals(Main.Money.franc(16), five.times(3));
    }

    public static void testEquality() {
        assertTrue(Main.Money.dollar(5).equals(Main.Money.dollar(5)));
        assertFalse(Main.Money.dollar(5).equals(Main.Money.dollar(6)));
        assertFalse(Main.Money.dollar(5).equals(Main.Money.franc(6)));
    }

    public static void testCurrency() {
        assertEquals("USD", Main.Money.dollar(1).currency());
        assertEquals("CHF", Main.Money.franc(1).currency());
        assertNotEquals("CHF", Main.Money.dollar(1).currency());
        assertNotEquals("USD", Main.Money.franc(1).currency());
    }

    public static void testDifferentClassEquality() {
        //assertTrue(new Main.Money(10, "CHF").equals(new Main.Franc(10, "CHF")));
        assertTrue(new Main.Money(10, "CHF").equals(new Main.Money(10, "CHF")));
    }

    public static void testSimpleAddition() {
        Main.Money five = Main.Money.dollar(5);
        Main.Expression sum = five.plus(five);
        Main.Bank bank = new Main.Bank();
        Main.Money reduced = bank.reduce(sum, "USD");
        assertEquals(Main.Money.dollar(10), reduced);
    }

    public static void testPlusReturnSum() {
        Main.Money five = Main.Money.dollar(5);
        Main.Expression result = five.plus(five);
        Main.Sum sum = (Main.Sum) result;
        assertEquals(five, sum.augend);
        assertEquals(five, sum.addend);
    }

    public static void testReduceSum() {
        Main.Expression sum = new Main.Sum(Main.Money.dollar(3), Main.Money.dollar(4));
        Main.Bank bank = new Main.Bank();
        Main.Money result = bank.reduce(sum, "USD");
        assertEquals(Main.Money.dollar(7), result);
    }

    public static void testReduceMoney() {
        Main.Bank bank = new Main.Bank();
        Main.Money result = bank.reduce(Main.Money.dollar(1), "USD");
        assertEquals(Main.Money.dollar(1), result);
    }

    public static void testReduceDifferentCurrency() {
        Main.Bank bank = new Main.Bank();
        bank.addRate("CHF", "USD", 2);
        Main.Money result = bank.reduce(Main.Money.franc(2), "USD");
        assertEquals(Main.Money.dollar(1), result);
    }

    public static void testIdentityRate() {
        assertEquals(1, new Main.Bank().rate("USD", "USD"));
    }

    public static void testMixedAddition() {
        Main.Expression fiveBucks= Main.Money.dollar(5);
        Main.Expression tenFrancs= Main.Money.franc(10);
        Main.Bank bank= new Main.Bank();
        bank.addRate("CHF", "USD", 2);
        Main.Money result= bank.reduce(fiveBucks.plus(tenFrancs), "USD");
        assertEquals(Main.Money.dollar(10), result);
    }

    public static void testSumPlusMoney() {
        Main.Expression fiveBucks= Main.Money.dollar(5);
        Main.Expression tenFrancs= Main.Money.franc(10);
        Main.Bank bank= new Main.Bank();
        bank.addRate("CHF", "USD", 2);
        Main.Expression sum= new Main.Sum(fiveBucks, tenFrancs).plus(fiveBucks);
        Main.Money result= bank.reduce(sum, "USD");
        assertEquals(Main.Money.dollar(15), result);
    }

    public static void testSumTimes() {
        Main.Expression fiveBucks= Main.Money.dollar(5);
        Main.Expression tenFrancs= Main.Money.franc(10);
        Main.Bank bank= new Main.Bank();
        bank.addRate("CHF", "USD", 2);
        Main.Expression sum= new Main.Sum(fiveBucks, tenFrancs).times(2);
        Main.Money result= bank.reduce(sum, "USD");
        assertEquals(Main.Money.dollar(20), result);
    }


    /*
    public static void testPlusSameCurrencyReturnsMoney() {
        Expression sum= Money.dollar(1).plus(Money.dollar(1));
        assertTrue(sum instanceof Money);
    }
     */


}
