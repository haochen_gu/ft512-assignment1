package money;

import java.util.Hashtable;

import static money.moneyTests.*;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Main {


    interface Expression {
        Money reduce(Bank bank, String to);
        Expression plus(Expression addend);
        Expression times(int multiplier);
    }

    private static class Pair {

        private String from;
        private String to;

        Pair(String from, String to) {
            this.from= from;
            this.to= to;
        }

        public boolean equals(Object object) {
            Pair pair= (Pair) object;
            return from.equals(pair.from) && to.equals(pair.to);
        }

        public int hashCode() {
            return 0;
        }


    }

    static class Bank {

        private Hashtable rates= new Hashtable();

        Money reduce(Expression source, String to) {
            return source.reduce(this, to);
        }

        void addRate(String from, String to, int rate) {
            rates.put(new Pair(from, to), new Integer(rate));
        }

        int rate(String from, String to) {
            if (from.equals(to)) {
                return 1;
            }
            Integer rate= (Integer) rates.get(new Pair(from, to));
            return rate.intValue();
        }
    }

    static class Sum implements Expression {
        Expression augend;
        Expression addend;

        Sum(Expression augend, Expression addend) {
            this.augend= augend;
            this.addend= addend;
        }

        public Money reduce(Bank bank, String to) {
            int amount= augend.reduce(bank, to).amount + addend.reduce(bank, to).amount;
            return new Money(amount, to);
        }

        public Expression plus(Expression addend) {
            return new Sum(this, addend);
        }

        public Expression times(int multiplier) {
            return new Sum(augend.times(multiplier),addend.times(multiplier));
        }
    }

    static class Money implements Expression{

        protected int amount;
        protected String currency;

        Money(int amount, String currency) {
            this.amount = amount;
            this.currency = currency;
        }

        public boolean equals(Object object) {
            Money money = (Money) object;
            return amount == money.amount && currency().equals(money.currency());
        }

        static Money dollar(int amount) {
            return new Money(amount, "USD");
        }

        static Money franc(int amount) {
            return new Money(amount, "CHF");
        }

        String currency() {
            return currency;
        }

        public String toString() {
            return amount + " " + currency;
        }

        public Expression times(int multiplier) {
            return new Money(amount * multiplier, currency);
        }

        public Expression plus(Expression addend) {
            return new Sum(this, addend);
        }

        public Money reduce(Bank bank, String to) {
            int rate = bank.rate(currency, to);
            return new Money(amount / rate, to);
        }

    }


    // main function to implement all the tests
    public static void main(String[] args) {
        testMultiplication();
        testEquality();
        testFrancMultiplication();
        testCurrency();
        testDifferentClassEquality();
        testSimpleAddition();
        testPlusReturnSum();
        testReduceSum();
        testReduceMoney();
        testReduceDifferentCurrency();
        testIdentityRate();
        testMixedAddition();
        testSumPlusMoney();
        testSumTimes();
        //testPlusSameCurrencyReturnsMoney();
    }

}
